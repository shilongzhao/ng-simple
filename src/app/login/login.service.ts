import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class LoginService {
  public token: string;
  constructor( @Inject('API_URL') private url: string, private http: Http) { }

  doLogin(username: string, password: string) {
    let params = new URLSearchParams();
    params.set('username', username);
    params.set('password', password);
    // TODO: this depends on the returned data structure from server. we need an interface
    return new Promise((resolve, reject) => {
      this.http.post(`${this.url}/auth/login`, params)
        .map(res => res.json().token) 
        .subscribe(data => {
          this.token = data;
          resolve(data);
        }, error => {
          reject(error);
        });
    });
  }
  /**
   * Only for test
   * @param username 
   * @param password 
   */
  testLogin(username: string, password: string) {
    return new Promise((resolve, reject) => {
      if (username === 'admin' && password === 'admin') {
        const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE0OTIxNTIxNTAsImV4cCI6MTUyMzY4ODE1MCwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsImZpcnN0bmFtZSI6IkpvaG5ueSIsImxhc3RuYW1lIjoiUm9ja2V0IiwiRW1haWwiOiJqcm9ja2V0QGV4YW1wbGUuY29tIiwiUm9sZSI6WyJNYW5hZ2VyIiwiUHJvamVjdCBBZG1pbmlzdHJhdG9yIl19.PHIh0fVzpbTqi8h74stfts_CqgEmku-j0NV5G1iS0BI'
        resolve(token);
      } else {
        reject('Invalid username/password');
      }
    });
  }
}
