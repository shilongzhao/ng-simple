import { Injectable } from '@angular/core';

import { default as swal, SweetAlertType, SweetAlertOptions } from 'sweetalert2';

@Injectable()
export class AlertService {

  constructor() { }

  error(text = 'Error') {

    const option: SweetAlertOptions = {
      title: 'Error',
      text: text,
      type: 'error',
      confirmButtonText: 'OK'
    };
    swal(option);

  }

  success(title = 'Done', text = '') {

    const option: SweetAlertOptions = {
      title: title,
      text: text,
      timer: 3000,
      type: 'success',
      confirmButtonText: 'OK'
    };
    swal(option)
      .then(() => { });

  }

  serverError() {

    const option: SweetAlertOptions = {
      title: 'Error',
      text: 'Error connecting to the server',
      type: 'error',
      confirmButtonText: 'OK'
    };
    swal(option);

  }

  confirm(text = 'Do you want to do this?', ) {
    const option: SweetAlertOptions = {
      title: 'Are you sure?',
      text: text,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, take action!',
      cancelButtonText: 'Cancel'
    };
     return swal(option);
  }
}
