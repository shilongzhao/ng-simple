import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ClarityModule } from 'clarity-angular';

import { AdminRoutingModule } from './admin-routing.module';
import { MainPageComponent } from './main-page/main-page.component';
import { HelperModule } from '../helper/helper.module';
import { AuthModule } from '../auth/auth.module';

import { MainService } from './main.service';
import { AlertService } from '../alert.service';
import { LayoutComponent } from './layout/layout.component';
import { ApartmentComponent } from './apartment/apartment.component';
import { ApartmentService } from 'app/admin/apartment.service';
import { HttpClientModule } from '@angular/common/http';
import { LoginService } from 'app/login/login.service';
import { ApartmentDetailComponent } from './apartment-detail/apartment-detail.component';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    AdminRoutingModule,
    HelperModule,
    FormsModule,
    ClarityModule.forRoot(),
    AuthModule,
  ],
  declarations: [MainPageComponent, LayoutComponent, ApartmentComponent, ApartmentDetailComponent],
  providers: [
    MainService,
    AlertService,
    ApartmentService,
    LoginService
  ]
})
export class AdminModule { }
