export interface Apartment {
    id: number;
    zipCode: string;
    price: number;
    area: number;
    street: string;
    streetNo: string;
    floor: number;
    enabled: boolean;
    version: string;
    // other detailed attributes to be added in future
}