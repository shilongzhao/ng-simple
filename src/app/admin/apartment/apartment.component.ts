import { Component, OnInit } from '@angular/core';
import { ApartmentQuery } from 'app/admin/apartment/ApartmentQuery';
import { ApartmentService } from 'app/admin/apartment.service';
import { Apartment } from 'app/admin/apartment/Apartment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-apartment',
  templateUrl: './apartment.component.html',
  styleUrls: ['./apartment.component.css']
})
export class ApartmentComponent implements OnInit {
  query = new ApartmentQuery();
  apartments: Array<Apartment> = new Array();
  selectedApartment: Apartment;
  constructor(private apartmentService: ApartmentService, private router: Router) { }

  ngOnInit() {
  }

  search() {
    this.apartmentService.searchApartment(this.query).subscribe(data => {
      console.log(data);
      this.apartments = data;
    });
  }
  onSelect(apartment: Apartment) {
    this.selectedApartment = apartment;
  }
  goDetail() {
    this.router.navigate(['admin/apartment', this.selectedApartment.id]);
  }
}
