export class ApartmentQuery {
    public id: number;
    public areaMin: number;
    public areaMax: number;
    public priceMin: number;
    public priceMax: number; 
    
    constructor(){}

    toString() {
        return "id: " + this.id + 
                " area: " + this.areaMax + " - " + this.areaMax + 
                " price: " + this.priceMin + " - " + this.priceMax;
    }
}