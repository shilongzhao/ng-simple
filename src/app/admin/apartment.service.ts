import { Injectable, Inject } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApartmentQuery } from 'app/admin/apartment/ApartmentQuery';
import { Apartment } from 'app/admin/apartment/Apartment';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginService } from 'app/login/login.service';
import { Observer } from 'rxjs/Observer';
import { Response } from '@angular/http';


@Injectable()
export class ApartmentService {  
  getApartment(id: number): Observable<Apartment> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8').append('Authorization', 'Bearer ' + this.loginService.token);
    return this.http.get<Apartment>(this.url + '/apartments/' + id, {headers: headers});
  }


  constructor(@Inject('API_URL') private url: string, private http: HttpClient, private loginService:LoginService) {}

  searchApartment(query: ApartmentQuery): Observable<Apartment[]> {
    console.log("searching apartments with token " + this.loginService.token);
    // HttpHeaders are immutable objects
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8').append('Authorization', 'Bearer ' + this.loginService.token);
    return this.http.post<Array<Apartment>>(this.url + '/apartments/search', query, {headers: headers});
  }

  updateApartment(apartment: Apartment): Observable<Apartment> {
    console.log("updating apartment " + JSON.stringify(apartment));
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8').append('Authorization', 'Bearer ' + this.loginService.token);
    return this.http.put<Apartment>(this.url + '/apartments/' + apartment.id , apartment, {headers: headers});
  }
}
