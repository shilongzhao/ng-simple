import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Apartment } from 'app/admin/apartment/Apartment';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ApartmentService } from 'app/admin/apartment.service';

@Component({
  selector: 'app-apartment-detail',
  templateUrl: './apartment-detail.component.html',
  styleUrls: ['./apartment-detail.component.css']
})
export class ApartmentDetailComponent implements OnInit {
  
  @Input() apartment: Apartment;
  editing: boolean;

  constructor(
    private route: ActivatedRoute, 
    private location: Location,
    private apartmentService: ApartmentService
  ) { }

  ngOnInit() {
    this.editing = false;
    this.getApartment();
  }

  getApartment() {
    const id = +this.route.snapshot.paramMap.get('id');
    console.log("Get info for id = " + id);
    this.apartmentService.getApartment(id).subscribe(a => {
      this.apartment = a;
      console.log("received data " + this.apartment);
    });
  }

  setEditing(status: boolean) {
    this.editing = status;
  }
  
  cancelEdit() {
    this.editing = false;
    console.log("Edit canceled!");
  }
  update() {
    this.apartmentService.updateApartment(this.apartment).subscribe(a =>{
      this.apartment = a;
      console.log("Modification saved!");
      this.editing = false;      
    })
  }
}
